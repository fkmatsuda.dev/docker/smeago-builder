# smeago-builder
The [smeago](https://gitlab.com/fkmatsuda.dev/smeago) project builder.

## Name
[smeago](https://gitlab.com/fkmatsuda.dev/smeago) Project Builder Docker Image

## Description
A docker image to build the [smeago](https://gitlab.com/fkmatsuda.dev/smeago) project.

## Usage
The intention of this image is just to provide the environment for the construction of the [smeago](https://gitlab.com/fkmatsuda.dev/smeago) project, but you can use it however you like if you find it useful.

## Roadmap
This project will be updated as per the [smeago](https://gitlab.com/fkmatsuda.dev/smeago) project requirements.

## License
This container container image contains:
* PostgreSQL is released under the [PostgreSQL License](./licenses/postgresql.md).
* pgTAP and pg_prove have a permissive license by [David E. Wheeler](./licenses/pgtap.md).
* Perl has a Permissive License by [Larry Wall and others](./licenses/perl.md).
* TAP::Formatter::JUnit follow the same terms as the Perl license by [Graham TerMarsch](./licenses/tap_formatter_junit.md).
* GO is distributed under a [BSD-style license](./licenses/golang.md).
* npm is released under the [Artistic License 2.0](./licenses/npm.md).
* Make is free software; you can redistribute it and/or modify it under the terms of the GNU [General Public License](./licenses/gpl.md) as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.


Other licenses may be applicable, I believe they are all open source, please excuse me for the lack of reference for all.
